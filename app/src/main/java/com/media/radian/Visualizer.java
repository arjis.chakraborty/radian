package com.media.radian;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.cleveroad.audiovisualization.AudioVisualization;
import com.cleveroad.audiovisualization.VisualizerDbmHandler;

public class Visualizer extends AppCompatActivity {
    
    MediaPlayer mMediaPlayer;
    AudioVisualization audioVisualization;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        audioVisualization = findViewById(R.id.visualizer_view);
        
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.MODIFY_AUDIO_SETTINGS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.MODIFY_AUDIO_SETTINGS}, 0);
        }
        
        mMediaPlayer = MediaPlayer.create(this, R.raw.music);
        
        VisualizerDbmHandler handler = VisualizerDbmHandler.Factory.newVisualizerHandler(this, mMediaPlayer);
        audioVisualization.linkTo(handler);
        
        mMediaPlayer.start();
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        mMediaPlayer.pause();
        mMediaPlayer.release();
    }
    
}
