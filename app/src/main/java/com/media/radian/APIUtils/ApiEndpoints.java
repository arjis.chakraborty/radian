package com.media.radian.APIUtils;

public class ApiEndpoints {
    
    public static final String MAIN_DOMAIN = "https://radian2021.herokuapp.com/api";
    public static final String USER_LOGIN = MAIN_DOMAIN + "/users/login";
    public static final String CHECK_USERNAME = MAIN_DOMAIN + "/users/checkUsername";
    public static final String USER_SIGNUP = MAIN_DOMAIN + "/users/signup";
    public static final String USER_VERIFICATION = MAIN_DOMAIN + "/users/checkVerification";
    public static final String CHECK_USER = MAIN_DOMAIN + "/users/checkUser";
    public static final String FORGOT_PASSWORD = MAIN_DOMAIN + "/users/forgotPassword";
    public static final String SEND_VERIFICATION_EMAIL = MAIN_DOMAIN + "/users/sendEmail";
    
    public static final String GET_ALL_MOODS = MAIN_DOMAIN + "/moods/getAllMoods";
    
    public static final String GET_SONGS = MAIN_DOMAIN + "/songs/getSongs";
    
    public static final String INITIATE = MAIN_DOMAIN + "/initiate/heatDynoDB";
    public static final String TERMS_CONDITIONS = "https://privacyterms.io/view/IqbpLesQ-sSRp87DA-7Yi4oU/";
    
}
