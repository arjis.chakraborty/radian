package com.media.radian.APIUtils;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.media.radian.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class RequestAPI {
    Context context;
    String endpoint;
    
    public static int MY_SOCKET_TIMEOUT_MS = 10000;
    
    public RequestAPI(Context context, String endpoint) {
        this.context = context;
        this.endpoint = endpoint;
    }
    
    public void post_req(final JSONObject requestParams, final ResponseCallBackInterface callBackInterface){
        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(com.android.volley.Request.Method.POST, endpoint, requestParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callBackInterface.response(response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callBackInterface.response(error.toString());
            }
        });
    
        request.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    
        queue.add(request);
    
    }
}
