package com.media.radian.Service.ServiceAdapters;

import android.app.PendingIntent;
import android.graphics.Bitmap;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;

public class NotificationDescriptionAdapter implements PlayerNotificationManager.MediaDescriptionAdapter {
    
    String title, bandName;
    
    public NotificationDescriptionAdapter(String title, String bandName) {
        this.title = title;
        this.bandName = bandName;
    }
    
    @Override
    public String getCurrentContentTitle(Player player) {
        return title;
    }
    
    @Nullable
    @Override
    public PendingIntent createCurrentContentIntent(Player player) {
        return null;
    }
    
    @Nullable
    @Override
    public String getCurrentContentText(Player player) {
        return bandName;
    }
    
    @Nullable
    @Override
    public String getCurrentSubText(Player player) {
        return null;
    }
    
    @Nullable
    @Override
    public Bitmap getCurrentLargeIcon(Player player, PlayerNotificationManager.BitmapCallback callback) {
        return null;
    }
}
