package com.media.radian.Service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadata;
import android.media.browse.MediaBrowser;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ControlDispatcher;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.audio.AudioListener;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceFactory;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import com.google.android.exoplayer2.util.Util;
import com.media.radian.Broadcast_Receivers.NotificationReceiver;
import com.media.radian.Interfaces.OnAudioStateChangedListener;
import com.media.radian.R;
import com.media.radian.Service.ServiceAdapters.NotificationDescriptionAdapter;
import com.media.radian.Utils.Utilities;
import com.media.radian.ViewAllSongs.SongsModel;
import com.media.radian.ViewAllSongs.ViewSongs_F;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.google.android.exoplayer2.DefaultLoadControl.DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS;
import static com.google.android.exoplayer2.DefaultLoadControl.DEFAULT_BUFFER_FOR_PLAYBACK_MS;
import static com.google.android.exoplayer2.DefaultLoadControl.DEFAULT_MAX_BUFFER_MS;
import static com.google.android.exoplayer2.DefaultLoadControl.DEFAULT_MIN_BUFFER_MS;
import static com.google.android.exoplayer2.DefaultLoadControl.DEFAULT_TARGET_BUFFER_BYTES;
import static com.media.radian.App.CHANNEL_1_ID;

public class AudioService extends Service implements OnAudioStateChangedListener {
    
    SimpleExoPlayer exoPlayer;
    boolean isPlaying = false;
    ArrayList<SongsModel> songsModelList;
    static int position;
    private NotificationManagerCompat notificationManager;
    private MediaSessionCompat mediaSession;
    String action;
    NotificationCompat.Builder notification = new NotificationCompat.Builder(AudioService.this, CHANNEL_1_ID);
    
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Utilities.setAudioListener(this);
        action = intent.getStringExtra("ACTION");
        mediaSession = new MediaSessionCompat(this, "tag");
        notificationManager = NotificationManagerCompat.from(this);
        songsModelList = new ArrayList<>();
        position = ViewSongs_F.position;
        if (songsModelList.size() == 0) {
            songsModelList = (ArrayList<SongsModel>) intent.getSerializableExtra("SONGS_ARRAY");
        }
        isPlaying = intent.getBooleanExtra("ISPLAYING", false);
        if (action != null)
            switch (action) {
                case "PLAY":
                    playSong();
                    showNotification();
                    break;
                case "PAUSE":
                    startFadeOut();
                    break;
                case "RESUME":
                    startFadeIn();
                    break;
                case "NEXT":
                    position++;
                    ViewSongs_F.position++;
                    ViewSongs_F.updateUI(position);
                    playSong();
                    action = "PLAY";
                    showNotification();
                    break;
                case "PREV":
                    position--;
                    ViewSongs_F.position--;
                    ViewSongs_F.updateUI(position);
                    playSong();
                    action = "PLAY";
                    showNotification();
                    break;
            }
        
        return START_STICKY;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }
    
    public void releasePlayer() {
        if (exoPlayer != null) {
            exoPlayer.release();
            exoPlayer = null;
            Utilities.setAudioPlaying(false);
        }
    }
    
    private void playSong() {
        if (!isPlaying) {
            releasePlayer();
            isPlaying = true;
            Utilities.setAudioPlaying(true);
        } else {
            exoPlayer.stop(false);
            exoPlayer.setPlayWhenReady(false);
            isPlaying = false;
            Utilities.setAudioPlaying(false);
        }
        initPlayer();
    }
    
    void initPlayer() {
        DefaultAllocator allocator = new DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE);
        DefaultLoadControl loadControl = new DefaultLoadControl(allocator, 30000,
                45000,
                1500,
                DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS,
                DEFAULT_TARGET_BUFFER_BYTES, true);
        exoPlayer = new SimpleExoPlayer.Builder(this)
                .setTrackSelector(new DefaultTrackSelector())
                .setLoadControl(loadControl)
                .setBandwidthMeter(new DefaultBandwidthMeter())
                .build();
        DataSource.Factory dataSource = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "Radian"));
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setContentType(C.CONTENT_TYPE_MUSIC).build();
        exoPlayer.setAudioAttributes(audioAttributes);
        exoPlayer.setPlayWhenReady(true);
        MediaSource mediaSource = new DefaultMediaSourceFactory(dataSource).createMediaSource(Uri.parse(songsModelList.get(position).getSongUrl()));
        
        exoPlayer.prepare(mediaSource);
        
        exoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int state) {
                Log.e("STATE", state + "");
                if (state == Player.STATE_READY) {
                
                }
                if (state == Player.STATE_ENDED) {
                    int nextPos = position + 1;
                    if (nextPos != songsModelList.size()) {
                        position++;
                        releasePlayer();
                        isPlaying = false;
                        playSong();
                        showNotification();
                    }
                }
            }
        });
    }
    
    void pausePlayer() {
        if (exoPlayer != null) {
            exoPlayer.setPlayWhenReady(false);
            showNotification();
            Utilities.setAudioPlaying(false);
        }
    }
    
    void resumePlayer() {
        if (exoPlayer != null) {
            exoPlayer.setPlayWhenReady(true);
            showNotification();
            Utilities.setAudioPlaying(true);
        }
    }
    
    float volume = 0;
    
    private void startFadeIn() {
        final int FADE_DURATION = 250; //The duration of the fade
        //The amount of time between volume changes. The smaller this is, the smoother the fade
        final int FADE_INTERVAL = 50;
        final int MAX_VOLUME = 1; //The volume will increase from 0 to 1
        int numberOfSteps = FADE_DURATION / FADE_INTERVAL; //Calculate the number of fade steps
        //Calculate by how much the volume changes each step
        final float deltaVolume = MAX_VOLUME / (float) numberOfSteps;
        
        //Create a new Timer and Timer task to run the fading outside the main UI thread
        final Timer timer = new Timer(true);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                resumePlayer();
                fadeInStep(deltaVolume); //Do a fade step
                //Cancel and Purge the Timer if the desired volume has been reached
                if (volume >= 1f) {
                    timer.cancel();
                    timer.purge();
                }
            }
        };
        
        timer.schedule(timerTask, FADE_INTERVAL, FADE_INTERVAL);
    }
    
    private void fadeInStep(float deltaVolume) {
        exoPlayer.setVolume(volume);
        volume += deltaVolume;
    }
    
    private void startFadeOut() {
        volume = 1;
        // The duration of the fade
        final int FADE_DURATION = 250;
        
        // The amount of time between volume changes. The smaller this is, the smoother the fade
        final int FADE_INTERVAL = 50;
        
        // Calculate the number of fade steps
        int numberOfSteps = FADE_DURATION / FADE_INTERVAL;
        
        // Calculate by how much the volume changes each step
        final float deltaVolume = volume / numberOfSteps;
        
        // Create a new Timer and Timer task to run the fading outside the main UI thread
        final Timer timer = new Timer(true);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                
                //Do a fade step
                fadeOutStep(deltaVolume);
                
                //Cancel and Purge the Timer if the desired volume has been reached
                if (volume <= 0) {
                    timer.cancel();
                    timer.purge();
                    pausePlayer();
                }
            }
        };
        
        timer.schedule(timerTask, FADE_INTERVAL, FADE_INTERVAL);
    }
    
    private void fadeOutStep(float deltaVolume) {
        exoPlayer.setVolume(volume);
        volume -= deltaVolume;
    }
    
    @SuppressLint("RestrictedApi")
    void showNotification() {
        String title = songsModelList.get(position).getSongName();
        String message = songsModelList.get(position).getSongArtist();
        if(Utilities.AUDIO_ARTWORK == null){
            GetBitmap getBitmap = new GetBitmap(title, message, action);
            getBitmap.execute();
        }
        else{
            Intent playPauseIntent = new Intent(AudioService.this, NotificationReceiver.class);
            if (!Utilities.IS_AUDIO_PLAYING)
                playPauseIntent.setAction("RESUME");
            else
                playPauseIntent.setAction("PAUSE");
            Intent previousIntent = new Intent(AudioService.this, NotificationReceiver.class);
            previousIntent.setAction("PREV");
            Intent nextIntent = new Intent(AudioService.this, NotificationReceiver.class);
            nextIntent.setAction("NEXT");
    
            PendingIntent playPausePIntent = PendingIntent.getBroadcast(AudioService.this, 1000, playPauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent nextPIntent = PendingIntent.getBroadcast(AudioService.this, 1001, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent prevPIntent = PendingIntent.getBroadcast(AudioService.this, 1002, previousIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    
            notification.mActions.clear();
            notification.setNotificationSilent();
    
            int pos = position;
            int prevPos = pos - 1;
            int nextPos = pos + 1;
            if (action.equals("PLAY")) {
                if ((prevPos != -1) && (nextPos != songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_ic_skip_previous, "Previous", prevPIntent)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .addAction(R.drawable.exo_ic_skip_next, "Next", nextPIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1, 2)
                                    .setMediaSession(mediaSession.getSessionToken()))
                    
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
            
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos == -1) && (nextPos != songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .addAction(R.drawable.exo_ic_skip_next, "Next", nextPIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1)
                                    .setMediaSession(mediaSession.getSessionToken()))
                    
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
            
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos != -1) && (nextPos == songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_ic_skip_previous, "Previous", prevPIntent)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1)
                                    .setMediaSession(mediaSession.getSessionToken()))
                    
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
            
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos == -1) && (nextPos == songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0)
                                    .setMediaSession(mediaSession.getSessionToken()))
                    
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
            
                    notificationManager.notify(1, notification.build());
                }
            }
            if (action.equals("PAUSE")) {
                if ((prevPos != -1) && (nextPos != songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_ic_skip_previous, "Previous", prevPIntent)
                            .addAction(R.drawable.exo_icon_play, "Play", playPausePIntent)
                            .addAction(R.drawable.exo_ic_skip_next, "Next", nextPIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1, 2)
                                    .setMediaSession(mediaSession.getSessionToken()))
                    
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(true)
                            .setOngoing(false)
                            .setOnlyAlertOnce(true)
                            .build();
            
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos == -1) && (nextPos != songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_icon_play, "Play", playPausePIntent)
                            .addAction(R.drawable.exo_ic_skip_next, "Next", nextPIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1)
                                    .setMediaSession(mediaSession.getSessionToken()))
                    
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(true)
                            .setOngoing(false)
                            .setOnlyAlertOnce(true)
                            .build();
            
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos != -1) && (nextPos == songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_ic_skip_previous, "Previous", prevPIntent)
                            .addAction(R.drawable.exo_icon_play, "Play", playPausePIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1)
                                    .setMediaSession(mediaSession.getSessionToken()))
                    
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(true)
                            .setOngoing(false)
                            .setOnlyAlertOnce(true)
                            .build();
            
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos == -1) && (nextPos == songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_icon_play, "Play", playPausePIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0)
                                    .setMediaSession(mediaSession.getSessionToken()))
                    
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(true)
                            .setOngoing(false)
                            .setOnlyAlertOnce(true)
                            .build();
            
                    notificationManager.notify(1, notification.build());
                }
            }
            if (action.equals("RESUME")) {
                if ((prevPos != -1) && (nextPos != songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_ic_skip_previous, "Previous", prevPIntent)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .addAction(R.drawable.exo_ic_skip_next, "Next", nextPIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1, 2)
                                    .setMediaSession(mediaSession.getSessionToken()))
                    
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
            
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos == -1) && (nextPos != songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .addAction(R.drawable.exo_ic_skip_next, "Next", nextPIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1)
                                    .setMediaSession(mediaSession.getSessionToken()))
                    
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
            
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos != -1) && (nextPos == songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_ic_skip_previous, "Previous", prevPIntent)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1)
                                    .setMediaSession(mediaSession.getSessionToken()))
                    
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
            
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos == -1) && (nextPos == songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0)
                                    .setMediaSession(mediaSession.getSessionToken()))
                    
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
            
                    notificationManager.notify(1, notification.build());
                }
            }
        }
    }
    
    @Override
    public void onStateChanged(boolean newState) {
    
    }
    
    public class GetBitmap extends AsyncTask<String, Void, Bitmap> {
        
        String title, message, action;
        
        public GetBitmap(String title, String message, String action) {
            this.title = title;
            this.message = message;
            this.action = action;
        }
        
        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                URL url = new URL(songsModelList.get(position).getArtworkURL());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        
        @SuppressLint("RestrictedApi")
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            Utilities.AUDIO_ARTWORK = bitmap;
            Intent playPauseIntent = new Intent(AudioService.this, NotificationReceiver.class);
            if (!Utilities.IS_AUDIO_PLAYING)
                playPauseIntent.setAction("RESUME");
            else
                playPauseIntent.setAction("PAUSE");
            Intent previousIntent = new Intent(AudioService.this, NotificationReceiver.class);
            previousIntent.setAction("PREV");
            Intent nextIntent = new Intent(AudioService.this, NotificationReceiver.class);
            nextIntent.setAction("NEXT");
            
            PendingIntent playPausePIntent = PendingIntent.getBroadcast(AudioService.this, 1000, playPauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent nextPIntent = PendingIntent.getBroadcast(AudioService.this, 1001, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent prevPIntent = PendingIntent.getBroadcast(AudioService.this, 1002, previousIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            
            notification.mActions.clear();
            notification.setNotificationSilent();
            
            int pos = position;
            int prevPos = pos - 1;
            int nextPos = pos + 1;
            if (action.equals("PLAY")) {
                if ((prevPos != -1) && (nextPos != songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_ic_skip_previous, "Previous", prevPIntent)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .addAction(R.drawable.exo_ic_skip_next, "Next", nextPIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1, 2)
                                    .setMediaSession(mediaSession.getSessionToken()))
                            
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
                    
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos == -1) && (nextPos != songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .addAction(R.drawable.exo_ic_skip_next, "Next", nextPIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1)
                                    .setMediaSession(mediaSession.getSessionToken()))
                            
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
                    
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos != -1) && (nextPos == songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_ic_skip_previous, "Previous", prevPIntent)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1)
                                    .setMediaSession(mediaSession.getSessionToken()))
                            
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
                    
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos == -1) && (nextPos == songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0)
                                    .setMediaSession(mediaSession.getSessionToken()))
                            
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
                    
                    notificationManager.notify(1, notification.build());
                }
            }
            if (action.equals("PAUSE")) {
                if ((prevPos != -1) && (nextPos != songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_ic_skip_previous, "Previous", prevPIntent)
                            .addAction(R.drawable.exo_icon_play, "Play", playPausePIntent)
                            .addAction(R.drawable.exo_ic_skip_next, "Next", nextPIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1, 2)
                                    .setMediaSession(mediaSession.getSessionToken()))
                            
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(true)
                            .setOngoing(false)
                            .setOnlyAlertOnce(true)
                            .build();
                    
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos == -1) && (nextPos != songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_icon_play, "Play", playPausePIntent)
                            .addAction(R.drawable.exo_ic_skip_next, "Next", nextPIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1)
                                    .setMediaSession(mediaSession.getSessionToken()))
                            
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(true)
                            .setOngoing(false)
                            .setOnlyAlertOnce(true)
                            .build();
                    
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos != -1) && (nextPos == songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_ic_skip_previous, "Previous", prevPIntent)
                            .addAction(R.drawable.exo_icon_play, "Play", playPausePIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1)
                                    .setMediaSession(mediaSession.getSessionToken()))
                            
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(true)
                            .setOngoing(false)
                            .setOnlyAlertOnce(true)
                            .build();
                    
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos == -1) && (nextPos == songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_icon_play, "Play", playPausePIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0)
                                    .setMediaSession(mediaSession.getSessionToken()))
                            
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(true)
                            .setOngoing(false)
                            .setOnlyAlertOnce(true)
                            .build();
                    
                    notificationManager.notify(1, notification.build());
                }
            }
            if (action.equals("RESUME")) {
                if ((prevPos != -1) && (nextPos != songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_ic_skip_previous, "Previous", prevPIntent)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .addAction(R.drawable.exo_ic_skip_next, "Next", nextPIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1, 2)
                                    .setMediaSession(mediaSession.getSessionToken()))
                            
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
                    
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos == -1) && (nextPos != songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .addAction(R.drawable.exo_ic_skip_next, "Next", nextPIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1)
                                    .setMediaSession(mediaSession.getSessionToken()))
                            
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
                    
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos != -1) && (nextPos == songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_ic_skip_previous, "Previous", prevPIntent)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0, 1)
                                    .setMediaSession(mediaSession.getSessionToken()))
                            
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
                    
                    notificationManager.notify(1, notification.build());
                } else if ((prevPos == -1) && (nextPos == songsModelList.size())) {
                    notification
                            .setSmallIcon(R.drawable.login_logo)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setLargeIcon(Utilities.AUDIO_ARTWORK)
                            .addAction(R.drawable.exo_icon_pause, "Pause", playPausePIntent)
                            .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                    .setShowActionsInCompactView(0)
                                    .setMediaSession(mediaSession.getSessionToken()))
                            
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setOnlyAlertOnce(true)
                            .build();
                    
                    notificationManager.notify(1, notification.build());
                }
            }
        }
    }
    
    public class MyBinder extends Binder {
        public AudioService getService() {
            return AudioService.this;
        }
    }
}
