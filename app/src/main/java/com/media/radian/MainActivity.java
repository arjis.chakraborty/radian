package com.media.radian;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.media.radian.Broadcast_Receivers.NetworkReceiver;
import com.media.radian.Broadcast_Receivers.NotificationReceiver;
import com.media.radian.Home.Home_F;
import com.media.radian.Interfaces.OnAudioStateChangedListener;
import com.media.radian.Service.AudioService;
import com.media.radian.Utils.Utilities;
import com.media.radian.ViewAllSongs.ViewSongs_F;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity implements OnAudioStateChangedListener {
    LinearLayout bottomLayout;
    static ImageView playPauseBtn;
    
    public static int prevPosition = -1;
    NetworkReceiver networkReceiver;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomLayout = findViewById(R.id.bottomLayout);
        playPauseBtn = bottomLayout.findViewById(R.id.playPauseBtn);
        
        Utilities.setAudioListener(this);
        
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragContainer, new Home_F())
                .commit();
        
        networkReceiver = new NetworkReceiver();
        
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);
        
        playPauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pauseResumeAudio();
            }
        });
    }
    
    public void pauseResumeAudio() {
        Intent serviceIntent = new Intent(MainActivity.this, AudioService.class);
        if (Utilities.IS_AUDIO_PLAYING) {
            Picasso.get()
                    .load(R.drawable.ic_play_button_outlined)
                    .placeholder(R.drawable.ic_play_button_outlined)
                    .into(playPauseBtn);
            /**
             * pause service
             * */
            serviceIntent.putExtra("ACTION", "PAUSE");
            serviceIntent.putExtra("POSITION", ViewSongs_F.position);
            serviceIntent.putExtra("ISPLAYING", Utilities.IS_AUDIO_PLAYING);
            startService(serviceIntent);
            
            Utilities.setAudioPlaying(false);
        } else {
            Picasso.get()
                    .load(R.drawable.ic_pause_outlined)
                    .placeholder(R.drawable.ic_pause_outlined)
                    .into(playPauseBtn);
            
            /**
             * resume service
             */
            serviceIntent.putExtra("ACTION", "RESUME");
            serviceIntent.putExtra("POSITION", ViewSongs_F.position);
            serviceIntent.putExtra("ISPLAYING", Utilities.IS_AUDIO_PLAYING);
            startService(serviceIntent);
            
            Utilities.setAudioPlaying(true);
        }
    }
    
    public static void updateUI(boolean newState) {
        if (!newState) {
            Picasso.get()
                    .load(R.drawable.ic_play_button_outlined)
                    .placeholder(R.drawable.ic_play_button_outlined)
                    .into(playPauseBtn);
        } else {
            Picasso.get()
                    .load(R.drawable.ic_pause_outlined)
                    .placeholder(R.drawable.ic_pause_outlined)
                    .into(playPauseBtn);
        }
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkReceiver);
    }
    
    @Override
    public void onStateChanged(boolean newState) {
        Log.e("NEWSTATE", newState + "");
        if (!newState) {
            Picasso.get()
                    .load(R.drawable.ic_play_button_outlined)
                    .placeholder(R.drawable.ic_play_button_outlined)
                    .into(playPauseBtn);
        } else {
            Picasso.get()
                    .load(R.drawable.ic_pause_outlined)
                    .placeholder(R.drawable.ic_pause_outlined)
                    .into(playPauseBtn);
        }
    }
}