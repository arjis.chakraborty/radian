package com.media.radian.ViewAllSongs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.media.radian.MainActivity;
import com.media.radian.R;
import com.media.radian.Utils.Utilities;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import hari.bounceview.BounceView;

public class ViewAllSongsAdapter extends RecyclerView.Adapter<ViewAllSongsAdapter.ViewHolder> {
    ArrayList<SongsModel> songsModelList;
    Context context;
    onSongItemClicked onSongItemClicked;
    
    public ViewAllSongsAdapter(ArrayList<SongsModel> songsModelList, Context context, onSongItemClicked onSongItemClicked) {
        this.songsModelList = songsModelList;
        this.context = context;
        this.onSongItemClicked = onSongItemClicked;
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_song_details, parent, false));
    }
    
    @Override
    public int getItemCount() {
        return songsModelList.size();
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.songName.setText(songsModelList.get(position).getSongName());
        holder.songArtist.setText(songsModelList.get(position).getSongArtist());
        holder.listenCount.setText(songsModelList.get(position).getListenCount());
        if (songsModelList.get(position).getId().equals(Utilities.SHARED_PREFERENCES.getString(Utilities.LAST_PLAYED_AUDIO_ID, "")) && Utilities.IS_AUDIO_PLAYING) {
            MainActivity.prevPosition = position;
            ViewSongs_F.position = position;
        }
        if (position == songsModelList.size() - 1) {
            Picasso.get()
                    .load(songsModelList.get(position).getArtworkURL())
                    .fit()
                    .centerCrop()
                    .into(holder.artwork, new Callback() {
                        @Override
                        public void onSuccess() {
                            ViewSongs_F.loadingLayout.setVisibility(View.GONE);
                        }
                        
                        @Override
                        public void onError(Exception e) {
                        
                        }
                    });
        } else {
            Picasso.get()
                    .load(songsModelList.get(position).getArtworkURL())
                    .fit()
                    .centerCrop()
                    .into(holder.artwork);
        }
    }
    
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView songName, songArtist, listenCount;
        ImageView artwork;
        RelativeLayout mainLayout;
        
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            songName = itemView.findViewById(R.id.songName);
            songArtist = itemView.findViewById(R.id.bandName);
            artwork = itemView.findViewById(R.id.songImage);
            listenCount = itemView.findViewById(R.id.listenCount);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            
            BounceView.addAnimTo(mainLayout).setScaleForPopOutAnim(0.95f, 0.95f);
            
            mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onSongItemClicked.onItemClick(getAdapterPosition(), mainLayout);
                }
            });
            
        }
        
    }
}
