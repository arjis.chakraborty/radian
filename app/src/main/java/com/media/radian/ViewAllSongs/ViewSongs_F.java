package com.media.radian.ViewAllSongs;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.media.radian.APIUtils.ApiEndpoints;
import com.media.radian.APIUtils.RequestAPI;
import com.media.radian.APIUtils.ResponseCallBackInterface;
import com.media.radian.Interfaces.OnAudioStateChangedListener;
import com.media.radian.MainActivity;
import com.media.radian.R;
import com.media.radian.Service.AudioService;
import com.media.radian.Utils.Utilities;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.BIND_AUTO_CREATE;

public class ViewSongs_F extends Fragment implements View.OnClickListener, OnAudioStateChangedListener {
    Toolbar toolbar;
    String source, moodId, moodType;
    RecyclerView recyclerView;
    ViewAllSongsAdapter adapter;
    public static ArrayList<SongsModel> songsModelList;
    static RelativeLayout loadingLayout;
    TextView heading;
    static LinearLayout bottomLayout;
    public static int position = -1;
    static ImageView playPauseBtn;
    View view;
    public static MainActivity activity;
    AudioService audioService;
    
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (MainActivity) context;
    }
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_all, container, false);
        Utilities.setAudioListener(activity);
        Bundle bundle = getArguments();
        if (bundle != null) {
            source = bundle.getString("SOURCE");
            moodType = bundle.getString("MOODTYPE");
            moodId = bundle.getString("MOODID");
        }
        
        Utilities.SHARED_PREFERENCES = getActivity().getSharedPreferences(Utilities.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.bg_grey));
        TextView title = toolbar.findViewById(R.id.name);
        ImageView backBtn = toolbar.findViewById(R.id.buttons);
        TextView skip = toolbar.findViewById(R.id.skip);
        skip.setVisibility(View.GONE);
        backBtn.setOnClickListener(this);
        title.setText(source);
        
        heading = view.findViewById(R.id.heading);
        heading.setText(moodType);
        loadingLayout = view.findViewById(R.id.loadingLayout);
        songsModelList = new ArrayList<>();
        bottomLayout = activity.findViewById(R.id.bottomLayout);
        playPauseBtn = bottomLayout.findViewById(R.id.playPauseBtn);
        bottomLayout.setOnClickListener(this);
        recyclerView = view.findViewById(R.id.allSongsRecycler);
        
        playPauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUI(position);
            }
        });
    
        Intent serviceIntentToStart = new Intent(activity, AudioService.class);
        activity.bindService(serviceIntentToStart, conn, BIND_AUTO_CREATE);
        
        callApi();
        
        return view;
    }
    
    private ServiceConnection conn = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            audioService = ((AudioService.MyBinder)service).getService();
        }
        
        public void onServiceDisconnected(ComponentName className) {
            audioService = null;
        }
    };
    
    void callApi() {
        try {
            JSONObject params = new JSONObject();
            params.put("moodId", moodId);
            RequestAPI requestAPI = new RequestAPI(activity, ApiEndpoints.GET_SONGS);
            requestAPI.post_req(params, new ResponseCallBackInterface() {
                @Override
                public void response(String response) {
                    try {
                        JSONObject body = new JSONObject(response);
                        String code = body.optString("code");
                        if (code.equals("200")) {
                            JSONArray msg = body.optJSONArray("msg");
                            if (msg != null) {
                                for (int i = 0; i < msg.length(); i++) {
                                    SongsModel model = new SongsModel();
                                    JSONObject obj = msg.optJSONObject(i);
                                    model.setSongName(obj.optString("name"));
                                    model.setSongArtist(obj.optString("artist"));
                                    model.setListenCount(obj.optString("listenCount"));
                                    model.setArtworkURL(obj.optString("artworkURL"));
                                    model.setSongUrl(obj.optString("songURL"));
                                    model.setId(obj.optString("id"));
                                    songsModelList.add(model);
                                }
                                //loadingLayout.setVisibility(View.GONE);
                                adapter = new ViewAllSongsAdapter(songsModelList, activity, new onSongItemClicked() {
                                    @Override
                                    public void onItemClick(int position, View view) {
                                        switch (view.getId()) {
                                            case R.id.mainLayout:
                                                ViewSongs_F.position = position;
                                                updateUI(position);
                                                break;
                                            
                                        }
                                    }
                                });
                                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                recyclerView.setAdapter(adapter);
                            }
                        }
                    } catch (JSONException e) {
                        Log.e("VIEWSONGS", e.toString());
                    }
                }
            });
        } catch (JSONException e) {
            Log.e("VIEWSONGS", e.toString());
        }
    }
    
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttons:
                activity.onBackPressed();
                break;
        }
    }
    
    public static void updateUI(int position) {
        if (MainActivity.prevPosition != position) {
            Animation bottomUp = AnimationUtils.loadAnimation(activity, R.anim.bottom_up);
            bottomLayout.startAnimation(bottomUp);
            bottomLayout.setVisibility(View.VISIBLE);
            Utilities.setAudioPlaying(false);
            /**
             * destroy service
             * */
            Intent serviceIntentToDestroy = new Intent(activity, AudioService.class);
            activity.stopService(serviceIntentToDestroy);
            
            updateBottomLayout(position);
            /**
             * start service
             * */
            Intent serviceIntentToStart = new Intent(activity, AudioService.class);
            serviceIntentToStart.putExtra("ACTION", "PLAY");
            serviceIntentToStart.putExtra("POSITION", position);
            serviceIntentToStart.putExtra("ISPLAYING", Utilities.IS_AUDIO_PLAYING);
            serviceIntentToStart.putExtra("SONGS_ARRAY", songsModelList);
            activity.startService(serviceIntentToStart);
            
            Utilities.SHARED_PREFERENCES
                    .edit()
                    .putString(Utilities.LAST_PLAYED_AUDIO_ID, songsModelList.get(position).getId())
                    .apply();
            
            if (!Utilities.IS_AUDIO_PLAYING) {
                        
                Picasso.get()
                        .load(R.drawable.ic_pause_outlined)
                        .placeholder(R.drawable.ic_pause_outlined)
                        .fit()
                        .centerCrop()
                        .into(playPauseBtn);
            } else {
                
                Picasso.get()
                        .load(R.drawable.ic_play_button_outlined)
                        .placeholder(R.drawable.ic_play_button_outlined)
                        .fit()
                        .centerCrop()
                        .into(playPauseBtn);
                
            }
            /*if (MainActivity.prevPosition != -1) {
                Picasso.get()
                        .load(R.drawable.ic_play_button_outlined)
                        .placeholder(R.drawable.ic_play_button_outlined)
                        .fit()
                        .centerCrop()
                        .into((ImageView) recyclerView.getChildAt(MainActivity.prevPosition).findViewById(R.id.playPauseBtn));
            }*/
            MainActivity.prevPosition = position;
            Utilities.setAudioPlaying(true);
        } else {
            Intent serviceIntent = new Intent(activity, AudioService.class);
            if (Utilities.IS_AUDIO_PLAYING) {
                /**
                 * pause service
                 */
                serviceIntent.putExtra("ACTION", "PAUSE");
                serviceIntent.putExtra("ISPLAYING", Utilities.IS_AUDIO_PLAYING);
                serviceIntent.putExtra("SONGS_ARRAY", songsModelList);
                activity.startService(serviceIntent);
    
                Utilities.setAudioPlaying(false);
                
                Picasso.get()
                        .load(R.drawable.ic_play_button_outlined)
                        .placeholder(R.drawable.ic_play_button_outlined)
                        .fit()
                        .centerCrop()
                        .into(playPauseBtn);
            } else {
                /**
                 * resume service
                 */
                serviceIntent.putExtra("ACTION", "RESUME");
                serviceIntent.putExtra("ISPLAYING", Utilities.IS_AUDIO_PLAYING);
                serviceIntent.putExtra("SONGS_ARRAY", songsModelList);
                activity.startService(serviceIntent);
    
                Utilities.setAudioPlaying(true);
                        
                Picasso.get()
                        .load(R.drawable.ic_pause_outlined)
                        .placeholder(R.drawable.ic_pause_outlined)
                        .fit()
                        .centerCrop()
                        .into(playPauseBtn);
            }
        }
    }
    
    static void updateBottomLayout(int position) {
        TextView songName = bottomLayout.findViewById(R.id.songName);
        TextView songArtist = bottomLayout.findViewById(R.id.bandName);
        ImageView artwork = bottomLayout.findViewById(R.id.songImage);
        songName.setText(songsModelList.get(position).getSongName());
        songArtist.setText(songsModelList.get(position).getSongArtist());
        Picasso.get()
                .load(songsModelList.get(position).getArtworkURL())
                .placeholder(R.drawable.ic_loading_placeholder)
                .fit()
                .centerCrop()
                .into(artwork);
    }
    
    @Override
    public void onStateChanged(boolean newState) {
        if (!newState) {
            Picasso.get()
                    .load(R.drawable.ic_play_button_outlined)
                    .placeholder(R.drawable.ic_play_button_outlined)
                    .into(playPauseBtn);
        } else {
            Picasso.get()
                    .load(R.drawable.ic_pause_outlined)
                    .placeholder(R.drawable.ic_pause_outlined)
                    .into(playPauseBtn);
        }
    }
}