package com.media.radian.ViewAllSongs;

import java.io.Serializable;

public class SongsModel implements Serializable {
    String id, artworkURL, songName, songArtist, listenCount, songUrl, isSaved;
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getSongUrl() {
        return songUrl;
    }
    
    public void setSongUrl(String songUrl) {
        this.songUrl = songUrl;
    }
    
    public String getArtworkURL() {
        return artworkURL;
    }
    
    public void setArtworkURL(String artworkURL) {
        this.artworkURL = artworkURL;
    }
    
    public String getSongName() {
        return songName;
    }
    
    public void setSongName(String songName) {
        this.songName = songName;
    }
    
    public String getSongArtist() {
        return songArtist;
    }
    
    public void setSongArtist(String songArtist) {
        this.songArtist = songArtist;
    }
    
    public String getListenCount() {
        return listenCount;
    }
    
    public void setListenCount(String listenCount) {
        this.listenCount = listenCount;
    }
    
    public String getIsSaved() {
        return isSaved;
    }
    
    public void setIsSaved(String isSaved) {
        this.isSaved = isSaved;
    }
}
