package com.media.radian.ViewAllSongs;

import android.view.View;

public interface onSongItemClicked {
    void onItemClick(int position, View view);
}
