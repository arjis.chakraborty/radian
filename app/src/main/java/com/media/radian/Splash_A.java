package com.media.radian;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.media.radian.APIUtils.ApiEndpoints;
import com.media.radian.APIUtils.RequestAPI;
import com.media.radian.APIUtils.ResponseCallBackInterface;
import com.media.radian.Login.Login_A;
import com.media.radian.Utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

public class Splash_A extends AppCompatActivity {
    ImageView logo;
    ProgressBar loadingProgress;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.setFullScreen(this);
        setContentView(R.layout.activity_splash);
        
        logo = findViewById(R.id.app_icon);
        loadingProgress = findViewById(R.id.loadingProgress);
        
        Utilities.SHARED_PREFERENCES = getSharedPreferences(Utilities.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        
        initiateAPI();
        
        logo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
        logo.setVisibility(View.VISIBLE);
        
        new Handler()
                .postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadingProgress.setVisibility(View.VISIBLE);
                    }
                }, 1000);
    }
    
    public void initiateAPI() {
        RequestAPI requestAPI = new RequestAPI(this, ApiEndpoints.INITIATE);
        requestAPI.post_req(null, new ResponseCallBackInterface() {
            @Override
            public void response(String response) {
                try {
                    JSONObject resp = new JSONObject(response);
                    String code = resp.optString("code");
                    if (code.equals("200")) {
                        Intent intent;
                        if (Utilities.SHARED_PREFERENCES.getBoolean(Utilities.LOGGED_IN, false)) {
                            intent = new Intent(Splash_A.this, MainActivity.class);
                        } else {
                            intent = new Intent(Splash_A.this, Login_A.class);
                        }
                        startActivity(intent);
                        finish();
                    }
                } catch (JSONException e) {
                    Log.e("SPLASH", e.toString());
                }
            }
        });
    }
}
