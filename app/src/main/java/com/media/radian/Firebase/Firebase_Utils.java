package com.media.radian.Firebase;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.media.radian.Utils.Utilities;

public class Firebase_Utils {
    
    public static FirebaseFirestore fStore = FirebaseFirestore.getInstance();
    public static FirebaseAuth mAuth = FirebaseAuth.getInstance();
    public static FirebaseStorage fStorage = FirebaseStorage.getInstance();
    
    public static boolean signInUserEmail(Context context, String email, String password) {
        Utilities.SHARED_PREFERENCES = context.getSharedPreferences(Utilities.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Utilities.SHARED_PREFERENCES.edit().putBoolean(Utilities.SIGN_IN, true).apply();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Utilities.SHARED_PREFERENCES.edit().putBoolean(Utilities.SIGN_IN, false).apply();
                    }
                });
        
        return Utilities.SHARED_PREFERENCES.getBoolean(Utilities.SIGN_IN, false);
    }
    
    public static boolean signInUserGoogle(Context context){
        
        return false;
    }
    
}
