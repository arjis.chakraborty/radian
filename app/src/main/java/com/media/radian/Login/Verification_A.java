package com.media.radian.Login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.media.radian.APIUtils.ApiEndpoints;
import com.media.radian.APIUtils.RequestAPI;
import com.media.radian.APIUtils.ResponseCallBackInterface;
import com.media.radian.MainActivity;
import com.media.radian.R;
import com.media.radian.Utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class Verification_A extends AppCompatActivity {
    ProgressBar progressBar;
    int id;
    String email;
    TextView bodyText;
    CallVerificationApi api1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        
        Utilities.SHARED_PREFERENCES = getSharedPreferences(Utilities.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        
        progressBar = findViewById(R.id.progress);
        bodyText = findViewById(R.id.bodyText);
        id = getIntent().getIntExtra("USER_ID", 0);
        email = getIntent().getStringExtra("EMAIL");
        
        String first = "We've sent a verification email to ";
        String next = "<font color='#FF04AF'>" + email + "</font>";
        bodyText.setText(Html.fromHtml(first + next));
        
        api1 = new CallVerificationApi();
        api1.execute();
        
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        if(api1 == null || api1.isCancelled()){
            CallVerificationApi api = new CallVerificationApi();
            api.execute();
        }
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        api1.cancel(true);
    }
    
    public class CallVerificationApi extends AsyncTask<String, Void, String> {
        String code = "202";
        JSONObject body;
        @Override
        protected String doInBackground(String... strings) {
            try {
                RequestAPI requestAPI = new RequestAPI(getApplicationContext(), ApiEndpoints.USER_VERIFICATION);
                JSONObject jsonBody = new JSONObject();
                jsonBody.put("id", id + "");
                requestAPI.post_req(jsonBody, new ResponseCallBackInterface() {
                    @Override
                    public void response(String response) {
                        try {
                            body = new JSONObject(response);
                            code = body.optString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
            
            return code;
        }
        
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("200")) {
                new Handler()
                        .postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                CallVerificationApi.this.onPostExecute(doInBackground(null));
                            }
                        }, 2000);
            }
            else{
                progressBar.setVisibility(View.GONE);
                Toast.makeText(Verification_A.this, "Logged in", Toast.LENGTH_LONG).show();
                JSONObject msg = body.optJSONObject("msg");
                SharedPreferences.Editor editor = Utilities.SHARED_PREFERENCES.edit();
                editor.putString(Utilities.EMAIL, msg.optString("email"));
                editor.putString(Utilities.USERNAME, msg.optString("username"));
                editor.putString(Utilities.USER_ID, msg.optString("id"));
                editor.putBoolean(Utilities.LOGGED_IN, true);
                editor.apply();
                Intent intent = new Intent(Verification_A.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }
}
