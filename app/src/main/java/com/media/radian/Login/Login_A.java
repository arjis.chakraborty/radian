package com.media.radian.Login;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.protobuf.Api;
import com.media.radian.APIUtils.ApiEndpoints;
import com.media.radian.APIUtils.RequestAPI;
import com.media.radian.APIUtils.ResponseCallBackInterface;
import com.media.radian.MainActivity;
import com.media.radian.R;
import com.media.radian.Utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.internal.Util;

public class Login_A extends AppCompatActivity implements View.OnClickListener {
    ImageView backBtn;
    TextView title, skipBtn, signUpBtn, forgotPasswordBtn, verifyBtn;
    TextInputLayout email, password;
    Button loginBtn;
    ProgressBar loadingProgress, verifyProgress;
    String id = "";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Toolbar toolbar = findViewById(R.id.toolbar);
        
        Utilities.SHARED_PREFERENCES = getSharedPreferences(Utilities.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        
        backBtn = toolbar.findViewById(R.id.buttons);
        backBtn.setVisibility(View.GONE);
        title = toolbar.findViewById(R.id.name);
        title.setText(R.string.login);
        skipBtn = toolbar.findViewById(R.id.skip);
        signUpBtn = findViewById(R.id.signupBtn);
        forgotPasswordBtn = findViewById(R.id.forgotPasswordBtn);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        loadingProgress = findViewById(R.id.loadingProgress);
        loginBtn = findViewById(R.id.loginBtn);
        verifyBtn = findViewById(R.id.verifyBtn);
        verifyProgress = findViewById(R.id.verifyProgress);
        
        boolean showToast = getIntent().getBooleanExtra("EMAIL_SENT", false);
        
        if (showToast) {
            Toast.makeText(this, "We've sent you an email to reset your password.", Toast.LENGTH_LONG).show();
        }
        
        email.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            
            }
            
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                toggleLoginBtn(charSequence.toString(), password.getEditText().getText().toString());
            }
            
            @Override
            public void afterTextChanged(Editable editable) {
            
            }
        });
        
        password.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            
            }
            
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                toggleLoginBtn(email.getEditText().getText().toString(), charSequence.toString());
            }
            
            @Override
            public void afterTextChanged(Editable editable) {
            
            }
        });
        
        loginBtn.setOnClickListener(this);
        signUpBtn.setOnClickListener(this);
        forgotPasswordBtn.setOnClickListener(this);
        skipBtn.setOnClickListener(this);
        verifyBtn.setOnClickListener(this);
    }
    
    boolean doubleBackToExitPressedOnce = false;
    
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
    
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginBtn:
                callAPI(email.getEditText().getText().toString(), password.getEditText().getText().toString());
                //setting clickable of login button to false to prevent unnecessary api calls
                loginBtn.setEnabled(false);
                loginBtn.setClickable(false);
                loginBtn.setBackgroundColor(getResources().getColor(R.color.disabled_bg));
                Utilities.hideKeyboard(this);
                break;
            
            case R.id.signupBtn:
                Utilities.hideKeyboard(this);
                Intent intent = new Intent(this, SignUp_A.class);
                startActivity(intent);
                break;
            
            case R.id.forgotPasswordBtn:
                Utilities.hideKeyboard(this);
                Intent i = new Intent(this, ForgotPassword_A.class);
                startActivity(i);
                break;
            
            case R.id.skip:
                Utilities.hideKeyboard(this);
                showDialog();
                break;
                
            case R.id.verifyBtn:
                verifyBtn.setVisibility(View.GONE);
                verifyProgress.setVisibility(View.VISIBLE);
                callVerificationAPI();
                break;
        }
    }
    
    //toggles the login button clickable behaviour
    public void toggleLoginBtn(String email, String password) {
        if (email.isEmpty() || password.isEmpty()) {
            loginBtn.setEnabled(false);
            loginBtn.setClickable(false);
            loginBtn.setBackgroundColor(getResources().getColor(R.color.disabled_bg));
        } else {
            loginBtn.setEnabled(true);
            loginBtn.setClickable(true);
            loginBtn.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }
    }
    
    //call api to send verification email
    void callVerificationAPI(){
        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", id);
            jsonObject.put("email", email.getEditText().getText().toString());
            
            RequestAPI requestAPI = new RequestAPI(this, ApiEndpoints.SEND_VERIFICATION_EMAIL);
            requestAPI.post_req(jsonObject, new ResponseCallBackInterface() {
                @Override
                public void response(String response) {
                    try{
                        JSONObject body = new JSONObject(response);
                        String code = body.optString("code");
                        if(code.equals("200")){
                            verifyProgress.setVisibility(View.GONE);
                            Toast.makeText(Login_A.this, "An email has been sent to " + email.getEditText().getText().toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (JSONException e){
                        Log.e("LOGIN", e.toString());
                    }
                }
            });
        }
        catch(JSONException e){
            Log.e("LOGIN", e.toString());
        }
    }
    
    //call API to login user
    public void callAPI(final String email, String password) {
        loadingProgress.setVisibility(View.VISIBLE);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email", email);
            jsonObject.put("password", password);
            
            RequestAPI requestAPI = new RequestAPI(this, ApiEndpoints.USER_LOGIN);
            requestAPI.post_req(jsonObject, new ResponseCallBackInterface() {
                @Override
                public void response(String response) {
                    loadingProgress.setVisibility(View.GONE);
                    try {
                        JSONObject body = new JSONObject(response);
                        String code = body.optString("code");
                        if (code.equals("200")) {
                            Login_A.this.email.setError(null);
                            verifyBtn.setVisibility(View.GONE);
                            Toast.makeText(Login_A.this, "Logged in", Toast.LENGTH_LONG).show();
                            JSONObject msg = body.optJSONObject("msg");
                            SharedPreferences.Editor editor = Utilities.SHARED_PREFERENCES.edit();
                            editor.putString(Utilities.EMAIL, msg.optString("email"));
                            editor.putString(Utilities.USERNAME, msg.optString("username"));
                            editor.putString(Utilities.USER_ID, msg.optString("id"));
                            editor.putBoolean(Utilities.LOGGED_IN, true);
                            editor.apply();
                            Intent intent = new Intent(Login_A.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else if (code.equals("202")) {
                            Login_A.this.email.setError("This account is not verified.");
                            Login_A.this.email.setErrorIconDrawable(null);
                            JSONObject msg = body.optJSONObject("msg");
                            id = msg.optString("id");
                            verifyBtn.setVisibility(View.VISIBLE);
                        } else {
                            Toast.makeText(Login_A.this, "Email or password did not match!", Toast.LENGTH_SHORT).show();
                            Login_A.this.email.setError(null);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            Log.e("LOGIN", e.toString());
        }
        
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        toggleLoginBtn(email.getEditText().getText().toString(), password.getEditText().getText().toString());
    }
    
    public void showDialog() {
        View view = getLayoutInflater().inflate(R.layout.dialog_skip, null);
        Button dismiss, skip;
        dismiss = view.findViewById(R.id.dismissBtn);
        skip = view.findViewById(R.id.skipBtn);
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(view)
                .show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login_A.this, MainActivity.class);
                startActivity(intent);
                finish();
                dialog.dismiss();
            }
        });
    }
}