package com.media.radian.Login;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputLayout;
import com.google.protobuf.Api;
import com.media.radian.APIUtils.ApiEndpoints;
import com.media.radian.APIUtils.RequestAPI;
import com.media.radian.APIUtils.ResponseCallBackInterface;
import com.media.radian.R;
import com.media.radian.Utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPassword_A extends AppCompatActivity implements View.OnClickListener {
    
    TextInputLayout email;
    Button confirmBtn;
    ProgressBar progressBar;
    Toolbar toolbar;
    ImageView backBtn;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        
        email = findViewById(R.id.email);
        confirmBtn = findViewById(R.id.confirmBtn);
        progressBar = findViewById(R.id.loadingProgress);
        confirmBtn.setOnClickListener(this);
    
        toolbar = findViewById(R.id.toolbar);
        backBtn = toolbar.findViewById(R.id.buttons);
        
        email.setBoxStrokeErrorColor(ColorStateList.valueOf(Color.RED));
        email.setErrorTextColor(ColorStateList.valueOf(Color.RED));
        
        email.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        
            }
    
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (email.getEditText().getText().toString().isEmpty()) {
                    disableBtn();
                }
                else if (!email.getEditText().getText().toString().matches(Utilities.EMAIL_REGEX)){
                    email.setError("Please enter a valid email address");
                    disableBtn();
                }
                else {
                    email.setError(null);
                    enableBtn();
                }
            }
    
            @Override
            public void afterTextChanged(Editable editable) {
        
            }
        });
        
        backBtn.setOnClickListener(this);
    }
    
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirmBtn:
                hideBtn();
                showProgress();
                checkEmail();
                break;
                
            case R.id.buttons:
                onBackPressed();
                break;
        }
    }
    
    void disableBtn() {
        confirmBtn.setBackgroundColor(getResources().getColor(R.color.disabled_bg));
        confirmBtn.setClickable(false);
        confirmBtn.setEnabled(false);
    }
    
    void enableBtn() {
        confirmBtn.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        confirmBtn.setClickable(true);
        confirmBtn.setEnabled(true);
    }
    
    void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }
    
    void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }
    
    void hideBtn() {
        confirmBtn.setVisibility(View.GONE);
    }
    
    void showBtn() {
        confirmBtn.setVisibility(View.VISIBLE);
    }
    
    void checkEmail() {
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("email", email.getEditText().getText().toString());
            
            RequestAPI requestAPI = new RequestAPI(this, ApiEndpoints.CHECK_USER);
            requestAPI.post_req(jsonBody, new ResponseCallBackInterface() {
                @Override
                public void response(String response) {
                    try {
                        JSONObject body = new JSONObject(response);
                        String code = body.optString("code");
                        if (code.equals("200")) {
                            email.setError(null);
                            resetPassword();
                        } else {
                            email.setError("No user registered with this email");
                            hideProgress();
                            showBtn();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            Log.e("FORGOT_PASSWORD_A", e.toString());
        }
    }
    
    void resetPassword() {
        try{
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("email", email.getEditText().getText().toString());
            
            RequestAPI requestAPI = new RequestAPI(this, ApiEndpoints.FORGOT_PASSWORD);
            requestAPI.post_req(jsonBody, new ResponseCallBackInterface() {
                @Override
                public void response(String response) {
                    try{
                        JSONObject body = new JSONObject(response);
                        String code = body.optString("code");
                        if(code.equals("200")){
                            Intent intent = new Intent(ForgotPassword_A.this, Login_A.class);
                            intent.putExtra("EMAIL_SENT", true);
                            startActivity(intent);
                            finish();
                        }
                    }
                    catch(JSONException e){
                        Log.e("FORGOTPASSWORD", e.toString());
                    }
                }
            });
        }
        catch(JSONException e){
            Log.e("FORGOTPASSORD", e.toString());
        }
    }
}
