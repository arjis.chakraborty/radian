package com.media.radian.Login;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputLayout;
import com.media.radian.APIUtils.ApiEndpoints;
import com.media.radian.APIUtils.RequestAPI;
import com.media.radian.APIUtils.ResponseCallBackInterface;
import com.media.radian.R;
import com.media.radian.Utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SignUp_A extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    TextView appBarName;
    ImageView backBtn;
    Button signUpBtn;
    ProgressBar apiCallProgress;
    TextInputLayout email, password, confirm_password, username, name;
    boolean isUsernameAvailable, isValidEmail, isValidPassword, isValidName = false;
    ArrayList<String> suggestedUsernames;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_signup);
        
        toolbar = findViewById(R.id.toolbar);
        appBarName = toolbar.findViewById(R.id.name);
        backBtn = toolbar.findViewById(R.id.buttons);
        appBarName.setText(R.string.signUp);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        confirm_password = findViewById(R.id.confirm_password);
        username = findViewById(R.id.username);
        name = findViewById(R.id.nameLayout);
        signUpBtn = findViewById(R.id.signUpBtn);
        apiCallProgress = findViewById(R.id.apiCallProgress);
        suggestedUsernames = new ArrayList<>();
        
        email.setBoxStrokeErrorColor(ColorStateList.valueOf(Color.RED));
        email.setErrorTextColor(ColorStateList.valueOf(Color.RED));
        password.setBoxStrokeErrorColor(ColorStateList.valueOf(Color.RED));
        password.setErrorTextColor(ColorStateList.valueOf(Color.RED));
        confirm_password.setBoxStrokeErrorColor(ColorStateList.valueOf(Color.RED));
        confirm_password.setErrorTextColor(ColorStateList.valueOf(Color.RED));
        username.setBoxStrokeErrorColor(ColorStateList.valueOf(Color.RED));
        username.setErrorTextColor(ColorStateList.valueOf(Color.RED));
        name.setBoxStrokeErrorColor(ColorStateList.valueOf(Color.RED));
        name.setErrorTextColor(ColorStateList.valueOf(Color.RED));
        
        email.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            
            }
            
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                email.setError(null);
                validateEmail();
            }
            
            @Override
            public void afterTextChanged(Editable editable) {
            
            }
        });
        
        password.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            
            }
            
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                password.setError(null);
                confirm_password.setError(null);
            }
            
            @Override
            public void afterTextChanged(Editable editable) {
            
            }
        });
        
        confirm_password.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            
            }
            
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                password.setError(null);
                confirm_password.setError(null);
            }
            
            @Override
            public void afterTextChanged(Editable editable) {
            
            }
        });
        
        username.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            
            }
            
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                signUpBtn.setEnabled(false);
                signUpBtn.setClickable(false);
                signUpBtn.setBackgroundColor(getResources().getColor(R.color.disabled_bg));
                if (username.getEditText().getText().toString().length() >= 6) {
                    apiCallProgress.setVisibility(View.VISIBLE);
                    signUpBtn.setVisibility(View.GONE);
                    checkUsername(username.getEditText().getText().toString());
                } else {
                    username.setErrorEnabled(true);
                    username.setError("Username must have at-least 6 characters");
                }
            }
            
            @Override
            public void afterTextChanged(Editable editable) {
            
            }
        });
        
        name.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            
            }
            
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            
            }
            
            @Override
            public void afterTextChanged(Editable editable) {
            
            }
        });
        
        signUpBtn.setOnClickListener(this);
        backBtn.setOnClickListener(this);
    }
    
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signUpBtn:
                if (validatePassword() && validateName() && validateAll()) {
                    Utilities.hideKeyboard(this);
                    showDialog();
                }
                break;
                
            case R.id.buttons:
                onBackPressed();
                break;
        }
    }
    
    //toggles the signUp button clickable behaviour
    public void toggleSignUpBtn(String email, String password, String confirm_password, String username, boolean enable) {
        if (email.isEmpty() || password.isEmpty() || confirm_password.isEmpty() || username.isEmpty() || username.length() < 6 || !enable || !isValidEmail || !isValidPassword || !isValidName) {
            signUpBtn.setEnabled(false);
            signUpBtn.setClickable(false);
            signUpBtn.setBackgroundColor(getResources().getColor(R.color.disabled_bg));
        } else {
            signUpBtn.setEnabled(true);
            signUpBtn.setClickable(true);
            signUpBtn.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }
    }
    
    public void validateEmail() {
        if (!email.getEditText().getText().toString().matches(Utilities.EMAIL_REGEX)) {
            email.setError("Please enter a valid email address");
            isValidEmail = false;
        } else {
            isValidEmail = true;
            email.setError(null);
        }
        toggleSignUpBtn(email.getEditText().getText().toString(),
                password.getEditText().getText().toString(),
                confirm_password.getEditText().getText().toString(),
                username.getEditText().getText().toString(), isUsernameAvailable);
    }
    
    public boolean validatePassword() {
        if (!password.getEditText().getText().toString().equals(confirm_password.getEditText().getText().toString())) {
            confirm_password.setError("Passwords do not match");
            password.setError("Passwords do not match");
            isValidPassword = false;
        } else {
            isValidPassword = true;
            confirm_password.setError(null);
            password.setError(null);
        }
        return isValidPassword;
    }
    
    public boolean validateName() {
        if (name.getEditText().getText().toString().isEmpty()) {
            isValidName = false;
            name.setError("Name cannot be empty");
        } else {
            isValidName = true;
            name.setError(null);
        }
        return isValidName;
    }
    
    public boolean validateAll() {
        return !email.getEditText().getText().toString().isEmpty()
                && !password.getEditText().getText().toString().isEmpty()
                && !confirm_password.getEditText().getText().toString().isEmpty()
                && !username.getEditText().getText().toString().isEmpty()
                && !name.getEditText().getText().toString().isEmpty();
    }
    
    void checkUsername(final String userName) {
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("username", userName);
            if (suggestedUsernames.size() == 0) {
                jsonBody.put("genUsernames", "1");
            }
            
            RequestAPI requestAPI = new RequestAPI(this, ApiEndpoints.CHECK_USERNAME);
            requestAPI.post_req(jsonBody, new ResponseCallBackInterface() {
                @Override
                public void response(String response) {
                    try {
                        JSONObject resp = new JSONObject(response);
                        Log.e("RESP", response);
                        String code = resp.optString("code");
                        if (code.equals("200")) {
                            isUsernameAvailable = true;
                            username.setErrorEnabled(false);
                            username.setError(null);
                            signUpBtn.setEnabled(true);
                            signUpBtn.setClickable(true);
                            signUpBtn.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                            apiCallProgress.setVisibility(View.GONE);
                            signUpBtn.setVisibility(View.VISIBLE);
                        } else {
                            isUsernameAvailable = false;
                            JSONArray msg = resp.optJSONArray("msg");
                            if (msg != null) {
                                for (int i = 0; i < msg.length(); i++) {
                                    suggestedUsernames.add(msg.getString(i));
                                }
                            }
                            String usernames = "";
                            for (String names : suggestedUsernames) {
                                usernames = usernames + ", " + names;
                            }
                            usernames = usernames.substring(1);
                            username.setErrorEnabled(true);
                            username.setError("Username already taken. Suggested : " + usernames);
                            signUpBtn.setEnabled(false);
                            signUpBtn.setClickable(false);
                            signUpBtn.setBackgroundColor(getResources().getColor(R.color.disabled_bg));
                            apiCallProgress.setVisibility(View.GONE);
                            signUpBtn.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        Log.e("SIGN_UP", e.toString());
                    }
                    
                }
            });
            
        } catch (JSONException e) {
            Log.e("SIGN_UP", e.toString());
        }
    }
    
    public void showDialog() {
        View view = this.getLayoutInflater().inflate(R.layout.dialog_terms_conditions, null);
        final ProgressBar loadingProgress = view.findViewById(R.id.loadingProgress);
        final Button dismiss, signUp;
        final CheckBox checkbox = view.findViewById(R.id.checkbox);
        dismiss = view.findViewById(R.id.dismissBtn);
        signUp = view.findViewById(R.id.signUp);
        
        WebView webview = view.findViewById(R.id.webView);
        webview.loadUrl(ApiEndpoints.TERMS_CONDITIONS);
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                loadingProgress.setVisibility(View.GONE);
            }
        });
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(view)
                .show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    signUp.setEnabled(true);
                    signUp.setClickable(true);
                    signUp.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                } else {
                    signUp.setEnabled(false);
                    signUp.setClickable(false);
                    signUp.setBackgroundColor(getResources().getColor(R.color.disabled_bg));
                }
                signUp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        showProgressDialog();
                    }
                });
            }
        });
        
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        
        
    }
    
    void showProgressDialog() {
        View view = getLayoutInflater().inflate(R.layout.dialog_progress, null);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(view)
                .create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Utilities.hideKeyboard(this);
        callVerificationAPI(dialog);
    }
    
    void callVerificationAPI(final AlertDialog dialog) {
        try {
            dialog.show();
            email.setError(null);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("name", name.getEditText().getText().toString());
            jsonBody.put("email", email.getEditText().getText().toString());
            jsonBody.put("password", password.getEditText().getText().toString());
            jsonBody.put("username", username.getEditText().getText().toString());
            
            RequestAPI requestAPI = new RequestAPI(this, ApiEndpoints.USER_SIGNUP);
            requestAPI.post_req(jsonBody, new ResponseCallBackInterface() {
                @Override
                public void response(String response) {
                    try {
                        final JSONObject body = new JSONObject(response);
                        String code = body.optString("code");
                        if (code.equals("200")) {
                            TextView textView = dialog.findViewById(R.id.text);
                            textView.setText(R.string.sending_confirmation_email);
                            new Handler()
                                    .postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            dialog.dismiss();
                                            JSONObject msg = body.optJSONObject("msg");
                                            int id = msg.optInt("id");
                                            Intent intent = new Intent(SignUp_A.this, Verification_A.class);
                                            intent.putExtra("USER_ID", id);
                                            intent.putExtra("EMAIL", email.getEditText().getText().toString());
                                            startActivity(intent);
                                            finish();
                                        }
                                    }, 2000);
                        }
                        else if(code.equals("202")){
                            email.setError("User already exists!");
                            dialog.dismiss();
                        }
                        else {
                            Toast.makeText(SignUp_A.this, "Something went wrong! Try again later.", Toast.LENGTH_SHORT).show();
                            email.setError(null);
                        }
                    } catch (JSONException e) {
                        Log.e("SIGNUP", e.toString());
                    }
                    
                }
            });
        } catch (JSONException e) {
            Log.e("SIGNUP", e.toString());
        }
    }
}
