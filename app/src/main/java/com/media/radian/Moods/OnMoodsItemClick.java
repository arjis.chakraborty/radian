package com.media.radian.Moods;

public interface OnMoodsItemClick {
    void onItemClick(int position);
}
