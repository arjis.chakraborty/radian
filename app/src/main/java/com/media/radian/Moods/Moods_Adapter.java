package com.media.radian.Moods;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.media.radian.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Moods_Adapter extends RecyclerView.Adapter<Moods_Adapter.ViewHolder> {
    ArrayList<Moods_GetSet> moods;
    Context context;
    OnMoodsItemClick onMoodsItemClick;
    int imgLoadedNo = 1;
    
    public Moods_Adapter(Context context, ArrayList<Moods_GetSet> moods, OnMoodsItemClick onMoodsItemClick) {
        this.moods = moods;
        this.context = context;
        this.onMoodsItemClick = onMoodsItemClick;
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_moods, parent, false));
    }
    
    @Override
    public int getItemCount() {
        return moods.size();
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Picasso.get()
                .load(moods.get(position).getImgURL())
                .fit()
                .centerCrop()
                .into(holder.imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        imgLoadedNo++;
                        if(imgLoadedNo == moods.size()){
                            Moods_F.loadingLayout.setVisibility(View.GONE);
                        }
                    }
    
                    @Override
                    public void onError(Exception e) {
        
                    }
                });
        
        holder.textView.setText(moods.get(position).getName());
    }
    
    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView textView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
            textView = itemView.findViewById(R.id.itemName);
            
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onMoodsItemClick.onItemClick(getAdapterPosition());
                }
            });
        }
    }
}
