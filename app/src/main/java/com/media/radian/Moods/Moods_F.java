package com.media.radian.Moods;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.media.radian.APIUtils.ApiEndpoints;
import com.media.radian.APIUtils.RequestAPI;
import com.media.radian.APIUtils.ResponseCallBackInterface;
import com.media.radian.R;
import com.media.radian.Utils.Utilities;
import com.media.radian.ViewAllSongs.ViewSongs_F;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Moods_F extends Fragment {
    View view;
    RecyclerView recyclerView;
    Moods_Adapter adapter;
    ArrayList<Moods_GetSet> moods;
    public static RelativeLayout loadingLayout;
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_moods, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        loadingLayout = view.findViewById(R.id.loadingLayout);
        moods = new ArrayList<>();
        callAPI();
        
        return view;
    }
    
    void callAPI() {
        RequestAPI requestAPI = new RequestAPI(getContext(), ApiEndpoints.GET_ALL_MOODS);
        requestAPI.post_req(null, new ResponseCallBackInterface() {
            @Override
            public void response(String response) {
                Log.e("RESP", response);
                try {
                    JSONObject body = new JSONObject(response);
                    String code = body.optString("code");
                    if (code.equals("200")) {
                        JSONArray msg = body.optJSONArray("msg");
                        if (msg != null) {
                            for (int i = 0; i < msg.length(); i++) {
                                Moods_GetSet moods_getSet = new Moods_GetSet();
                                moods_getSet.setImgURL(msg.optJSONObject(i).optString("imgURL"));
                                moods_getSet.setName(msg.optJSONObject(i).optString("name"));
                                moods_getSet.setId(msg.optJSONObject(i).optString("id"));
                                moods.add(moods_getSet);
                            }
                            adapter = new Moods_Adapter(getContext(), moods, new OnMoodsItemClick() {
                                @Override
                                public void onItemClick(int position) {
                                    handleClick(position);
                                }
                            });
                            recyclerView.setLayoutManager(new Utilities.CenterZoomLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                            SnapHelper snapHelper = new PagerSnapHelper();
                            snapHelper.attachToRecyclerView(recyclerView);
                            recyclerView.setAdapter(adapter);
                        }
                    }
                } catch (JSONException e) {
                    Log.e("MOODS_F", e.toString());
                }
            }
        });
    }
    
    void handleClick(int position) {
        String moodId = moods.get(position).getId();
        String moodType = moods.get(position).getName();
        Bundle bundle = new Bundle();
        bundle.putString("MOODID", moodId);
        bundle.putString("MOODTYPE", moodType);
        bundle.putString("SOURCE", "Moods");
        ViewSongs_F viewSongsFragment = new ViewSongs_F();
        viewSongsFragment.setArguments(bundle);
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                .replace(R.id.fragContainer, viewSongsFragment)
                .addToBackStack(null)
                .commit();
    }
}
