package com.media.radian.Broadcast_Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.lifecycle.Lifecycle;

import com.media.radian.Interfaces.OnAudioStateChangedListener;
import com.media.radian.MainActivity;
import com.media.radian.Service.AudioService;
import com.media.radian.Utils.Utilities;
import com.media.radian.ViewAllSongs.ViewSongs_F;

public class NotificationReceiver extends BroadcastReceiver implements OnAudioStateChangedListener {
    
    @Override
    public void onReceive(Context context, Intent intent) {
        Utilities.setAudioListener(this);
        String action = intent.getAction();
        Intent serviceIntent = new Intent(context, AudioService.class);
        if (action != null) {
            switch (action) {
                case "RESUME":
                    serviceIntent.putExtra("ACTION", "RESUME");
                    serviceIntent.putExtra("ISPLAYING", Utilities.IS_AUDIO_PLAYING);
                    serviceIntent.putExtra("SONGS_ARRAY", ViewSongs_F.songsModelList);
                    serviceIntent.putExtra("POSITION", ViewSongs_F.position);
                    context.startService(serviceIntent);
                    
                    Utilities.setAudioPlaying(true);
                    break;
                
                case "PAUSE":
                    serviceIntent.putExtra("ACTION", "PAUSE");
                    serviceIntent.putExtra("ISPLAYING", Utilities.IS_AUDIO_PLAYING);
                    serviceIntent.putExtra("SONGS_ARRAY", ViewSongs_F.songsModelList);
                    serviceIntent.putExtra("POSITION", ViewSongs_F.position);
                    context.startService(serviceIntent);
                    
                    Utilities.setAudioPlaying(false);
                    break;
                
                case "PREV":
                    Utilities.AUDIO_ARTWORK = null;
                    serviceIntent.putExtra("ACTION", "PREV");
                    serviceIntent.putExtra("ISPLAYING", Utilities.IS_AUDIO_PLAYING);
                    serviceIntent.putExtra("SONGS_ARRAY", ViewSongs_F.songsModelList);
                    serviceIntent.putExtra("POSITION", ViewSongs_F.position);
                    context.startService(serviceIntent);
                    break;
                
                case "NEXT":
                    Utilities.AUDIO_ARTWORK = null;
                    serviceIntent.putExtra("ACTION", "NEXT");
                    serviceIntent.putExtra("ISPLAYING", Utilities.IS_AUDIO_PLAYING);
                    serviceIntent.putExtra("SONGS_ARRAY", ViewSongs_F.songsModelList);
                    serviceIntent.putExtra("POSITION", ViewSongs_F.position);
                    context.startService(serviceIntent);
                    break;
            }
        }
    }
    
    @Override
    public void onStateChanged(boolean newState) {
        Log.e("NEW_STATE_RECEIVER", newState + "");
        MainActivity.updateUI(newState);
    }
}
