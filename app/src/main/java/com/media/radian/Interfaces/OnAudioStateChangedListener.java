package com.media.radian.Interfaces;

public interface OnAudioStateChangedListener {
    void onStateChanged(boolean newState);
}
